# Restlet Example

[![Circle CI](https://img.shields.io/circleci/project/bitbucket/romeara/bds-circleci-testing-repo.svg)](https://circleci.com/bb/romeara/bds-circleci-testing-repo)

Provides an example of a simple Restlet application

## Examples in this Application

* Setup of a simple Restlet application with an end point
* Setup of dependency injection support for end point implementations using spring autowiring
